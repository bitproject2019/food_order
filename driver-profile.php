<?php
session_start();
//echo $_SESSION['user_id'];
//$_SESSION['user_id'] = $user_id;
if (!isset($_SESSION['user_id'],$_SESSION['user_type'])) {
    header('location:foodpicky/registration.php');
    exit;
}

?>
<?php 
include_once('header.php');
include_once('sidebar.php');
$connect = mysqli_connect("localhost", "root", "", "food_order");  
$query = "SELECT * FROM food";  
$result = mysqli_query($connect, $query);
?>


<div class="page-inner">
	<div class="page-title">
		<h3>Profile</h3>
		<div class="page-breadcrumb">
			<ol class="breadcrumb">
				
				
			</ol>
		</div>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">Add Profile</h4>
			</div>
			 
			 <div class="panel-body">
				<form class="form-horizontal" method="POST" id="profile" >
					<div class="form-group">
						<label for="txtfullname" class="col-sm-2 control-label">Full Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtfullname" name="txtfullname">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="txtnic">NIC</label>
						<div class="col-sm-10">
                            <input type="text" class="form-control" id="txtnic" name="txtnic">
	
						</div>
					</div>
					<div class="form-group">
						<label for="txtphone" class="col-sm-2 control-label">Phone</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtphone" name="txtphone">
						</div>
                    </div>
                    <div class="form-group">
						<label for="txtemail" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtemail" name="txtemail">
						</div>
                    </div>

                    <div class="form-group">
						<label for="txtvechicleno" class="col-sm-2 control-label">Vehicle No</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtvechicleno" name="txtvechicleno">
						</div>
					</div>
					<div class="form-group">
						<label for="txtlicence" class="col-sm-2 control-label">Licence No</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtlicence" name="txtlicence">
						</div>
					</div>

					<div class="form-group">
						<label for="txtvechiletype" class="col-sm-2 control-label">Vehicle Type</label>
						<div class="col-sm-10">
                        <select class="form-control m-b-sm" name="txtvechiletype" id="txtvechiletype">
                            <option value="bike">bike</option>
                            <option value="car">car</option>
                            <option value="three wheeler">three wheeler</option>
                            
                        </select>
                        </div>	
					</div>			
    		
				
					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-5">
							<button type="submit" class="btn btn-success btn-addon m-b-sm"><i class="fa fa-plus"></i> ADD TO WEBSITE</button>

						</div>
					</div>
			</form>	
		</div>	
		
		

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
    var data = 'Driver';
	$.ajax({

		method:'POST',
		url:"load/profile.php",
		data:data,
		dataType:"json",
		success:function(data)
		{
			console.log(data.vehicle_no);
            $('#txtfullname').val(data.full_name);
            $('#txtnic').val(data.nic_no);
            $('#txtphone').val(data.phone_number);
            $('#txtvechicleno').val(data.vehicle_no);
            $('#txtlicence').val(data.drive_licence);
            $('#txtemail').val(data.email);
            $('#txtvechiletype').val(data.vehicle_type);
		}

	})

    $('#profile').submit(function(e){
        e.preventDefault();
        // alert('hi');
        var txtuser = 'driver';
        var data = $(this).serialize() + "&txtuser="+txtuser;
        $.ajax({

            method:'POST',
            url:"load/update.php",
            data:data,
            dataType:"text",
            success:function(data)
            {
                console.log(data);
               
            }

        })
    })
	
	
});
</script>
<?php 
    include_once('footer.php');
?>

