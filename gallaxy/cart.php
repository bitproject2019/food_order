<?php
include_once('../load/connection.php');
session_start();
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    <title>LOGIN</title>
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animsition.min.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet"> 

    <meta charset="utf-8">
        <title>My Account Information |Kings Eats</title>
        <meta name="description" content="Gallaxy Responsive HTML5/CSS3 Template from FIFOTHEMES.COM">
        <meta name="author" content="FIFOTHEMES.COM">
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css'>
        <!-- Library CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/fonts/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="css/animations.css" media="screen">
        <link rel="stylesheet" href="css/superfish.css" media="screen">
        <link rel="stylesheet" href="css/team-member.css" media="screen">
        <link rel="stylesheet" href="css/prettyPhoto.css" media="screen">
        <!-- Theme CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Skin -->
        <link rel="stylesheet" href="css/colors/green.css" class="colors">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/theme-responsive.css">
        <!-- Switcher CSS -->
        <link href="css/switcher.css" rel="stylesheet">
        <link href="css/spectrum.css" rel="stylesheet">
        <!-- Favicons -->
        <link rel="shortcut icon" href="img/ico/favicon.ico">
        <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144.png">
</head>

<body>
     <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!--header starts-->
         <header id="header" class="header-scroll top-header headrom">
            <!-- .navbar -->
            <nav class="navbar navbar-dark">
               <div class="container">
                  <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#mainNavbarCollapse">&#9776;</button>
                  <a class="navbar-brand" href="index-2.html"> <img class="img-rounded" src="../images/lave logo.png" alt=""> </a>
                  <div class="collapse navbar-toggleable-md  float-lg-right" id="mainNavbarCollapse">
                     <ul class="nav navbar-nav">
                     <li class="nav-item"> <a class="nav-link active" href="index.php">Home 
                                <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                                    aria-haspopup="true" aria-expanded="false">Food</a>
                                <div class="dropdown-menu"> <a class="dropdown-item" href="../food_results.php">Food
                                        results</a>
                                </div>
                            </li>
                            <li class="nav-item"> <a class="nav-link " href="../foodpicky/registration.php">Login/sign 
                                <span class="sr-only"></span></a>
                            </li>
                            <?php
                                if(isset($_SESSION['user_type']))
                                {
                                    $location ='';
                                    if ($_SESSION['user_type']=='customer') {
                                  
                                        
                                        $location = 'dashboard-customer.php';
                                     
                                
                                    }
                                    if ($_SESSION['user_type']=='driver') {
                                  
                                        $location = '../all-order.php';

                                       
                                    } 
                                    if ($_SESSION['user_type']=='shop') {
                                        
                                        $location = '../add-food.php';

                                      
                                
                                    }
                                    if ($_SESSION['user_type']=='admin') {
                                        $location = '../add-food-admin.php';

                                      
                                
                                    }
                                    ?>

                                <li class="nav-item"> <a class="nav-link " href="<?php echo $location; ?>">Account 
                                    <span class="sr-only"></span></a>
                                </li>
                                <?php
                                }
                                ?>
                     </ul>
                  </div>
               </div>
            </nav>
            <!-- /.navbar -->
         </header>
         <div class="page-wrapper">
           
            </div>
          
            <section id="main">
                <!-- Title, Breadcrumb -->
                <div class="breadcrumb-wrapper">
                    <div class="pattern-overlay">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                    <h2 class="title">Shopping Cart</h2>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                    <div class="breadcrumbs pull-right">
                                        <ul>
                                            <li>You are Now on:</li>
                                            <li><a href="index-2.html">Home</a></li>
                                            <li><a href="shop.html">Shop</a></li>
                                            <li>Shopping Cart</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Title, Breadcrumb -->
                <!-- Main Content -->
                <?php
                    $user =  $_SESSION['user_id'];
                    $token = $_SESSION['token'];
                    $sql = mysqli_query($mysqli,"select * from `order` where status='Basket' and (user_id='$user' or temp_id='$token' )");
                    $row=mysqli_fetch_array($sql);
                    $food_id = $row['food_id'];   

                    $food = mysqli_query($mysqli,"select * from food where id='$food_id'");
                    $fooddata=mysqli_fetch_array($food);
                                   
                ?>
                <!-- <input type="text" name="userid" value="<?php  echo $user; ?>"> -->
                <div class="content margin-top60 margin-bottom60">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="table-box">
                                    <!-- shopping-cart-table -->
                                        <form action="" id="orderform">
                                    <table class="shopping-cart-table responsive-table table table-bordered table-striped" id="shopping-cart-table">
                                        <thead>
                                            <tr>
                                                
                                                <th class="td-name">Food Name</th>
                                                <th class="td-price">Shop</th>
                                                <th class="td-price">Unit Price</th>
                                                <th class="td-total">Location</th>
                                                <th class="td-qty">Qty</th>
                                                
                                                <th class="td-total">Subtotal</th>
                                                
                                                <th class="td-remove">Cancel</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="td-images">
                                                   <?php echo $fooddata['name']; ?>
                                                </td>
                                                <td class="td-name">
                                                    <?php echo $fooddata['shop_name']; ?>
                                                </td>
                                                <td class="td-edit unit">
                                                   <?php
                                                        $rate = '';
                                                        $price = "$fooddata[price]";
                                                        $discount = "$fooddata[discount]";
                                                        if($fooddata['discount_type'] == 'Percentage')
                                                        {
                                                            $rate .= $price - ($discount/100 * $price ); 
                                                        }
                                                        else if($fooddata['discount_type'] == 'Rupees')
                                                        {
                                                            $rate .= $price - $discount; 
                                                        }
                                                        echo $rate;

                                                    ?>
                                                    
                                                </td>
                                                <td class="td-total total">
                                              
                                                 <?php
                                                    if($row['id'] !='')
                                                    {
                                                 ?>
                                                 <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                         <textarea name="location" id="location" cols="30" rows="10" required></textarea>
                                                <?php

                                                    }
                                                ?>
                                                </td>
                                                <td class="td-price">
                                                <?php
                                                    if($row['id'] !='')
                                                    {
                                                 ?>
                                                        <input type="number" value="<?php echo $row['quantity']; ?>" name="quantity" class="form-control quantity" min='1' required>
                                                <?php

                                                    }
                                                ?>
                                                </td>
                                                    
                                                </td>
                                                
                                                <td class="td-total nettotal">
                                                    <div class="price"></div>
                                                </td>
                                                <td class="td-remove">
                                                <?php
                                                    if($row['id'] !='')
                                                    {
                                                 ?>
                                                    <a href="<?php echo $row['id']; ?>" id="delete">
                                                        <i class="fa fa-trash-o"></i>
                                                    </a>
                                                    <input type="hidden" name="total_rate" class="totalrate" >

                                                <?php

                                                    }
                                                ?>
                                                    
                                                    
                                                </td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                   
                                    <!-- /shopping-cart-table -->
                                    <?php
                                        //  }
                                    ?>
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="row" id="car-bottom">
                            <div class="col-md-8"></div>
                            <div class="col-sm-4 col-md-4">
                                <div class="car-bottom-box bg total">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h3>Grand Total : </h3>
                                                </td>
                                                <td >
                                                    <h3 class="nettotal"></h3>
                                                </td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                    <div>
                                        <button type="submit" class="btn  btn-color">Proceed to Checkout</button>
                                        <!-- <a class="btn  btn-color" href="checkout.html">Proceed to Checkout</a><br> -->
                                        
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Main Content -->
            </section>
            
            <!-- start: FOOTER -->
            <footer class="footer">
               <div class="container">
                  <!-- top footer statrs -->
                  <div class="row top-footer">
                     <div class="col-xs-12 col-sm-3 footer-logo-block color-gray">
                        <a href="#"> <img src="../images/food-picky-logo.png" alt="Footer logo"> </a> <span>Order Delivery &amp; Take-Out </span> 
                     </div>
                     <div class="col-xs-12 col-sm-2 about color-gray">
                        <h5>About Us</h5>
                        <ul>
                           <li><a href="#">About us</a> </li>
                           <li><a href="#">History</a> </li>
                           <li><a href="#">Our Team</a> </li>
                           <li><a href="#">We are hiring</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-2 how-it-works-links color-gray">
                        <h5>How it Works</h5>
                        <ul>
                           <li><a href="#">Enter your location</a> </li>
                           <li><a href="#">Choose restaurant</a> </li>
                           <li><a href="#">Choose meal</a> </li>
                           <li><a href="#">Pay via credit card</a> </li>
                           <li><a href="#">Wait for delivery</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-2 pages color-gray">
                        <h5>Pages</h5>
                        <ul>
                           <li><a href="#">Search results page</a> </li>
                           <li><a href="#">User Sing Up Page</a> </li>
                           <li><a href="#">Pricing page</a> </li>
                           <li><a href="#">Make order</a> </li>
                           <li><a href="#">Add to cart</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-3 popular-locations color-gray">
                        <h5>Popular locations</h5>
                        <ul>
                           <li><a href="#">Jaffna town</a> </li>
                           <li><a href="#">Nallur</a> </li>
                           <li><a href="#">kandy Road</a> </li>
                           <li><a href="#">Kokuvil</a> </li>
                           <li><a href="#">KSS Road</a> </li>
                           <li><a href="#">Kopay</a> </li>
                           <li><a href="#">Palai</a> </li>
                           
                        </ul>
                     </div>
                  </div>
                  <!-- top footer ends -->
                  <!-- bottom footer statrs -->
                  <div class="row bottom-footer">
                     <div class="container">
                        <div class="row">
                           <div class="col-xs-12 col-sm-3 payment-options color-gray">
                              <h5>Payment Options</h5>
                              <ul>
                                 <li>
                                    <a href="#"> <img src="../images/paypal.png" alt="Paypal"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/mastercard.png" alt="Mastercard"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/maestro.png" alt="Maestro"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/stripe.png" alt="Stripe"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/bitcoin.png" alt="Bitcoin"> </a>
                                 </li>
                              </ul>
                           </div>
                           <div class="col-xs-12 col-sm-4 address color-gray">
                              <h5>Address</h5>
                              <p>Concept design of oline food order and deliveye,planned as restaurant directory</p>
                              <h5>Phone: <a href="tel:+080000012222">080 000012 222</a></h5>
                           </div>
                           <div class="col-xs-12 col-sm-5 additional-info color-gray">
                              <h5>Addition informations</h5>
                              <p>Join the thousands of other restaurants who benefit from having their menus on TakeOff</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- bottom footer ends -->
               </div>
            </footer>
            <!-- end:Footer -->
         </div>
         <!-- end:page wrapper -->
      </div>
      <!--/end:Site wrapper -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/tether.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/animsition.min.js"></script>
    <script src="../js/bootstrap-slider.min.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/headroom.js"></script>
    <script src="../js/foodpicky.min.js"></script>
    <script src="js/jquery-migrate-1.0.0.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/revolution-slider/js/jquery.themepunch.plugins.min.js"></script> 
    <script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/jquery.parallax.js"></script>
    <script src="js/jquery.wait.js"></script>
    <script src="js/fappear.js"></script> 
    <script src="js/modernizr-2.6.2.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/tweetMachine.js"></script>
    <script src="js/tytabs.js"></script>
    <script src="js/jquery.gmap.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jflickrfeed.js"></script>
    <script src="js/jquery.knob.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/spectrum.js"></script>
    <script src="js/switcher.js"></script>
    <script src="js/custom.js"></script>

    <script>
     $(function () {
        //    $('.total').prop('readonly', true);
            var $tblrows = $("#shopping-cart-table tr");

            $tblrows.each(function (index) {
                var $tblrow = $(this);
                function totalfunction(){
                    var qty = $tblrow.find(".quantity").val();
                    var price = $tblrow.find(".unit").html();
                    var subTotal = parseInt(qty, 10) * parseFloat(price);

                    if (!isNaN(subTotal)) {

                        $tblrow.find('.total').val(subTotal.toFixed(2));
                        $tblrow.find('.rateprice').html(subTotal.toFixed(2));

                        var grandTotal = 0;

                        $(".total").each(function () {
                            var stval = parseFloat($(this).val());
                            grandTotal += isNaN(stval) ? 0 : stval;
                        });
                        
                        // alert( grandTotal.toFixed(2));


                        $('.nettotal').text('Rs '+grandTotal.toFixed(2));
                        $('.totalrate').val(grandTotal.toFixed(2));

                    }
                }
                $tblrow.find('.quantity').on('change', function () {
                    totalfunction();
                        
                });
            
                $(document).ready(function () {
                    totalfunction();
                });
            });

        });

        $(document).ready(function () {
            $('#delete').click(function(e){
                e.preventDefault();
                var data = $(this).attr('href');
                $.ajax({

                    method:'POST',
                    url:"../load/delete.php",
                    //data:data,
                    data: {data:data},
                    dataType:"text",
                    success:function(data)
                    {
                        console.log(data);

                    }

                })
            })

            $('#orderform').submit(function(e){
                e.preventDefault();
                // alert('dsd');
                var data = $(this).serialize();
                // alert(data);
                $.ajax({

                    method:'POST',
                    url:"../load/update-order.php",
                    //data:data,
                    data: data,
                    dataType:"text",
                    success:function(data)
                    {
                        window.location.assign('/gallaxy/payment.php');

                    }

                })
            })
        });
    </script>
</body>


</html>