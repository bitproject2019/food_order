<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    <title>LOGIN</title>
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animsition.min.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet"> 

    <meta charset="utf-8">
        <title>My Account Information |Kings Eats</title>
        <meta name="description" content="Gallaxy Responsive HTML5/CSS3 Template from FIFOTHEMES.COM">
        <meta name="author" content="FIFOTHEMES.COM">
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css'>
        <!-- Library CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/fonts/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="css/animations.css" media="screen">
        <link rel="stylesheet" href="css/superfish.css" media="screen">
        <link rel="stylesheet" href="css/team-member.css" media="screen">
        <link rel="stylesheet" href="css/prettyPhoto.css" media="screen">
        <!-- Theme CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Skin -->
        <link rel="stylesheet" href="css/colors/green.css" class="colors">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/theme-responsive.css">
        <!-- Switcher CSS -->
        <link href="css/switcher.css" rel="stylesheet">
        <link href="css/spectrum.css" rel="stylesheet">
        <!-- Favicons -->
        <link rel="shortcut icon" href="img/ico/favicon.ico">
        <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144.png">
</head>

<body>
     <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!--header starts-->
         <header id="header" class="header-scroll top-header headrom">
            <!-- .navbar -->
            <nav class="navbar navbar-dark">
               <div class="container">
                  <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#mainNavbarCollapse">&#9776;</button>
                  <a class="navbar-brand" href="index-2.html"> <img class="img-rounded" src="../images/lave logo.png" alt=""> </a>
                  <div class="collapse navbar-toggleable-md  float-lg-right" id="mainNavbarCollapse">
                     <ul class="nav navbar-nav">
                        <li class="nav-item"> <a class="nav-link active" href="index-2.html">Home <span class="sr-only">(current)</span></a> </li>
                        <li class="nav-item"> <a class="nav-link active" href="index-2.html">Food <span class="sr-only">(current)</span></a> </li>
                        <li class="nav-item"> <a class="nav-link active" href="index-2.html"><button type="button" class="btn btn-default btn-rounded">Login</button> <span class="sr-only">(current)</span></a> </li>
                     </ul>
                  </div>
               </div>
            </nav>
            <!-- /.navbar -->
         </header>
         <div class="page-wrapper">
           
            </div>
            <section class="main">
            <div class="breadcrumb-wrapper">
                    <div class="pattern-overlay">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                    <h2 class="title">Edit Account Information</h2>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                    <div class="breadcrumbs pull-right">
                                        <ul>
                                            <li>You are Now on:</li>
                                            <li><a href="index-2.html">Home</a></li>
                                            <li><a href="shop.html">Shop</a></li>
                                            <li>Edit Account Information</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Main Content -->
                <div class="content margin-top60 margin-bottom60">
                    <div class="container">
                        <div class="row">
                                <div id="sidebar" class="sidebar col-sm-3 col-md-3 col-lg-3">
                                        <div class="">
                                            <h3>My Account</h3>
                                            <!-- menu-->
                                            <div id="sidebar-nav">
                                                <ul class="sidebar-nav">
                                                <li >
                                                    <a href="dashboard-customer.php"><i class="fa fa-gears item-icon"></i>My Dashboard</a>
                                                </li>
                                                <li class="active">
                                                    <a href="account-info.php"><i class="fa fa-user item-icon"></i>Account Information</a>
                                                </li>
                                                <!-- <li>
                                                    <a href="my-address.html"><i class="fa fa-pencil-square-o item-icon"></i>Address Book</a>
                                                </li> -->
                                                <li>
                                                    <a href="cart.php"><i class="fa fa-shopping-cart item-icon"></i>My Cart</a>
                                                </li>
                                                </ul>
                                            </div>
                                            <!-- /menu-->
                                        </div>
                                    </div>
                            <!-- Left Section -->
                            <div class="col-sm-9 col-md-9 col-lg-9">
                                <div class="my-account margin-top">
                                    <div class="row">
                                    <div class="buttons-box clearfix">
                                        <span class="required pull-right"><b>*</b> Required Field</span>
                                    </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="title-box">
                                                <h3>Account Information</h3>
                                            </div>
                                            <form action="" method="post" id="accountform">
                                                <ul class="list-unstyled">
                                                    
                                                    <li>
                                                        <div class="form-group">
                                                            <label for="txtfullname">Full Name <span class="required">*</span></label>
                                                            <input type="text" name="txtfullname" id="txtfullname" class="form-control" placeholder="Full name"  required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="txtnic">NIC <span class="required">*</span></label>
                                                            <input type="text" name="txtnic" id="txtnic" class="form-control" placeholder="NIC" required>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group">
                                                            <label for="txtemail">Email Address <span class="required">*</span></label>
                                                            <input type="email" name="txtemail" id="txtemail" class="form-control" placeholder="Email" required>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group">
                                                            <label for="txtphone">Phone No <span class="required">*</span></label>
                                                            <input type="text" name="txtphone" id="txtphone" class="form-control" placeholder="Phone no" required>
                                                        </div>
                                                    </li>
                                                    <li>
                                                    <div class="buttons-box clearfix">
                                                        <button class="btn btn-color" type="submit">Save</button>
                                                    </div>
                                                    </li>
                                                </ul>
                                            </form>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="title-box">
                                                <h3>Change Password</h3>
                                            </div>
                                            <form action="" method="post" id="security">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div class="form-group">
                                                            <label for="txtoldpass">Current Password <span class="required">*</span></label>
                                                            <input type="password" name="txtoldpass" id="txtoldpass" class="form-control" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="txtpass">New Password <span class="required">*</span></label>
                                                            <input type="password" name="txtpass" id="txtpass" class="form-control" required>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group">
                                                            <label for="txtcpass">Confirm New Password <span class="required">*</span></label>
                                                            <input type="password" name="txtcpass" id="txtcpass" class="form-control" required>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group">
                                                        <button class="btn btn-color" type="submit">Save</button>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </form>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <!-- /Left Section -->
                            <!-- Sidebar -->
                            
                            <!-- /Sidebar -->
                        </div>
                    </div>
                </div>
            </section>
            
            <!-- start: FOOTER -->
            <footer class="footer">
               <div class="container">
                  <!-- top footer statrs -->
                  <div class="row top-footer">
                     <div class="col-xs-12 col-sm-3 footer-logo-block color-gray">
                        <a href="#"> <img src="../images/food-picky-logo.png" alt="Footer logo"> </a> <span>Order Delivery &amp; Take-Out </span> 
                     </div>
                     <div class="col-xs-12 col-sm-2 about color-gray">
                        <h5>About Us</h5>
                        <ul>
                           <li><a href="#">About us</a> </li>
                           <li><a href="#">History</a> </li>
                           <li><a href="#">Our Team</a> </li>
                           <li><a href="#">We are hiring</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-2 how-it-works-links color-gray">
                        <h5>How it Works</h5>
                        <ul>
                           <li><a href="#">Enter your location</a> </li>
                           <li><a href="#">Choose restaurant</a> </li>
                           <li><a href="#">Choose meal</a> </li>
                           <li><a href="#">Pay via credit card</a> </li>
                           <li><a href="#">Wait for delivery</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-2 pages color-gray">
                        <h5>Pages</h5>
                        <ul>
                           <li><a href="#">Search results page</a> </li>
                           <li><a href="#">User Sing Up Page</a> </li>
                           <li><a href="#">Pricing page</a> </li>
                           <li><a href="#">Make order</a> </li>
                           <li><a href="#">Add to cart</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-3 popular-locations color-gray">
                        <h5>Popular locations</h5>
                        <ul>
                           <li><a href="#">Jaffna town</a> </li>
                           <li><a href="#">Nallur</a> </li>
                           <li><a href="#">kandy Road</a> </li>
                           <li><a href="#">Kokuvil</a> </li>
                           <li><a href="#">KSS Road</a> </li>
                           <li><a href="#">Kopay</a> </li>
                           <li><a href="#">Palai</a> </li>
                           
                        </ul>
                     </div>
                  </div>
                  <!-- top footer ends -->
                  <!-- bottom footer statrs -->
                  <div class="row bottom-footer">
                     <div class="container">
                        <div class="row">
                           <div class="col-xs-12 col-sm-3 payment-options color-gray">
                              <h5>Payment Options</h5>
                              <ul>
                                 <li>
                                    <a href="#"> <img src="../images/paypal.png" alt="Paypal"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/mastercard.png" alt="Mastercard"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/maestro.png" alt="Maestro"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/stripe.png" alt="Stripe"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="../images/bitcoin.png" alt="Bitcoin"> </a>
                                 </li>
                              </ul>
                           </div>
                           <div class="col-xs-12 col-sm-4 address color-gray">
                              <h5>Address</h5>
                              <p>Concept design of oline food order and deliveye,planned as restaurant directory</p>
                              <h5>Phone: <a href="tel:+080000012222">080 000012 222</a></h5>
                           </div>
                           <div class="col-xs-12 col-sm-5 additional-info color-gray">
                              <h5>Addition informations</h5>
                              <p>Join the thousands of other restaurants who benefit from having their menus on TakeOff</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- bottom footer ends -->
               </div>
            </footer>
            <!-- end:Footer -->
         </div>
         <!-- end:page wrapper -->
      </div>
      <!--/end:Site wrapper -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/tether.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/animsition.min.js"></script>
    <script src="../js/bootstrap-slider.min.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/headroom.js"></script>
    <script src="../js/foodpicky.min.js"></script>
    <script src="js/jquery-migrate-1.0.0.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/revolution-slider/js/jquery.themepunch.plugins.min.js"></script> 
    <script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/jquery.parallax.js"></script>
    <script src="js/jquery.wait.js"></script>
    <script src="js/fappear.js"></script> 
    <script src="js/modernizr-2.6.2.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/tweetMachine.js"></script>
    <script src="js/tytabs.js"></script>
    <script src="js/jquery.gmap.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jflickrfeed.js"></script>
    <script src="js/jquery.knob.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/spectrum.js"></script>
    <script src="js/switcher.js"></script>
    <script src="js/custom.js"></script>

    <script>
    $(document).ready(function () {
        var data = 'Customer';
        $.ajax({

            method:'POST',
            url:"../load/profile.php",
            data:data,
            dataType:"json",
            success:function(data)
            {
                console.log(data);
                $('#txtfullname').val(data.full_name);
                $('#txtnic').val(data.nic_no);
                $('#txtphone').val(data.phone_number);
                $('#txtemail').val(data.txtemail);
            }
        });

        $('#security').submit(function(e){
            e.preventDefault();
            // alert('hi');
            var txtuser = 'password';
            
            var data = $(this).serialize() + "&txtuser="+txtuser;
            // console.log(data);
            $.ajax({

                method:'POST',
                url:"../load/update.php",
                //data:data,
                data: data,
                // dataType:"text",
                // contentType:false,
                // cache:false,
                // processData:false,
                dataType:"text",
                success:function(data)
                {
                    console.log(data);
                
                }

            })
        })

        $('#accountform').submit(function(e){
            e.preventDefault();
            // alert('hi');
            var txtuser = 'customer';
            
            var data = $(this).serialize() + "&txtuser="+txtuser;
            // console.log(data);
            $.ajax({

                method:'POST',
                url:"../load/update.php",
                //data:data,
                data: data,
                // dataType:"text",
                // contentType:false,
                // cache:false,
                // processData:false,
                dataType:"text",
                success:function(data)
                {
                    console.log(data);
                
                }

            })
        })
    });
    </script>
</body>


</html>