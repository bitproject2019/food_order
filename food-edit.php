<?php 
session_start();
include_once('header.php');
include_once('sidebar.php');
// extract($_POST);/
// print_r();
// $foodid = $_POST['foodid'];
include_once('load/connection.php');
$output = '';

    $sql = $mysqli->query("select * from food where id='$_GET[id]' ");
    $output = '';
    while ($data = $sql->fetch_array()) {
	

?>


<div class="page-inner">
	<div class="page-title">
		<h3>Admin</h3>
		<div class="page-breadcrumb">
			<ol class="breadcrumb">
				
				
			</ol>
		</div>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">ADD FOOD</h4>
			</div>
			 
			 <div class="panel-body">
				<form class="form-horizontal" method="POST" id="updateform" action="load/foodupdate.php" enctype="multipart/form-data" >
				<input type="hidden" name="id" value="<?php echo $data['id']; ?>">
					<div class="form-group"> 
						<label for="txtfood" class="col-sm-2 control-label">Food Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtfood" name="txtfood" value="<?php 	echo $data['name']; ?>">
						</div>
					</div>
					<?php 	echo $data['time']; ?>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="txttime">Time</label>
						<div class="col-sm-10">
							<select class="form-control m-b-sm" multiple name="txttime" id="txttime" >
								<option <?php echo($data['time']) == 'Breakfast' ? 'selected' : '' ?> value="Breakfast">BREAKFAST</option>
								<option <?php echo($data['time']) == 'Lunch' ? 'selected' : '' ?> value="Lunch">LUNCH</option>
								<option <?php echo($data['time']) == 'Tea Time' ? 'selected' : '' ?> value="Tea Time">TEA TIME</option>
								<option <?php echo($data['time']) == 'Dinner' ? 'selected' : '' ?> value="Dinner">DINNER</option>
							</select>
							
						</div>
					</div>
					<div class="form-group">
						<label for="txtshopname" class="col-sm-2 control-label">Shop Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtshopname" name="txtshopname" value="<?php 	echo $data['shop_name']; ?>">
						</div>
						</div>

						<div class="form-group">
						<label for="txtshopaddress" class="col-sm-2 control-label">Shop Location</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtshopaddress" name="txtshopaddress" value="<?php 	echo $data['shop_location']; ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="txtprice" class="col-sm-2 control-label">Food Price</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtprice" name="txtprice" value="<?php 	echo $data['price']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="txtdiscount" class="col-sm-2 control-label">Discount</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="txtdiscount" name="txtdiscount" value="<?php 	echo $data['discount']; ?>">
						</div>
						<div class="col-sm-5">
							<select class="form-control m-b-sm" name="txtdiscounttype" id="txtdiscounttype">
								<option value="Percentage">%</option>
								<option value="Rupees">RS</option>
								
							</select>
							
						</div>
					</div>

								
    				<div class="form-group">
						<label for="txtimage" class="col-sm-2 control-label">Add Image</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" id="file" name="file">
						</div>
					</div>
					<div class="form-group">
					<div class="col-sm-2"></div>

						<div class="col-sm-10">
						<img src="upload/<?php echo $data['image']; ?>" alt="" width="100px" height="100px">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-5">
							<button type="submit" name="foodupdate" class="btn btn-success btn-addon m-b-sm"><i class="fa fa-plus"></i> Update TO WEBSITE</button>

						</div>
					</div>
			</form>	
		</div>	
	
<?php 

}
    include_once('footer.php');
?>
<script src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script>
// $(document).on('submit','#updateform',function(e){
// 	e.preventDefault();
// //    alert('hi');
// //    var foodupdate = 'foodupdate'
// //    var formdata = new FormData(this);
// //    $.ajax({
// // 		url:"load/update.php",
// // 		type:"POST"
// // 		data:{formdata:formdata,foodupdate:foodupdate},
// // 		dataType:"Json",
// // 		contentType:false,
// // 		cache:false,
// // 		processData:false,
// //    })
// //    .done(function (res) {
// // 	   console.log(res);
// //    })
	
// });
 </script>  


