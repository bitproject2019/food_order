<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/foodpicky/registration.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Mar 2019 16:46:12 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    <title>LOGIN</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animsition.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet"> </head>
<body>
     <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!--header starts-->
         <header id="header" class="header-scroll top-header headrom">
            <!-- .navbar -->
            <nav class="navbar navbar-dark">
               <div class="container">
                  <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#mainNavbarCollapse">&#9776;</button>
                  <a class="navbar-brand" href="index.php"> <img class="img-rounded" src="images/lave logo.png" alt=""> </a>
                  <div class="collapse navbar-toggleable-md  float-lg-right" id="mainNavbarCollapse">
                     <ul class="nav navbar-nav">
                        <li class="nav-item"> <a class="nav-link active" href="index.php">Home <span class="sr-only">(current)</span></a> </li>
                        <li class="nav-item"> <a class="nav-link active" href="index-2.html">Food <span class="sr-only">(current)</span></a> </li>
                        <li class="nav-item"> <a class="nav-link active" href="registration.php"><button type="button" class="btn btn-default btn-rounded">Login</button> <span class="sr-only">(current)</span></a> </li>
                     </ul>
                  </div>
               </div>
            </nav>
            <!-- /.navbar -->
         </header>
         <div class="page-wrapper">
           
            </div>
            <section class="contact-page inner-page">
               <div class="container">
                  <div class="row">
                  
                     <!-- REGISTER -->
                     <div class="col-md-6">
                     <h3>REGISTER</h3>
                        <div class="widget">
                           <div class="widget-body">
                              <form method="POST">
                                 <div class="row">
                                    <div class="row">
                                        <div class="form-group col-sm-8">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-secondary">
                                            <input type="radio" name="txtuser" id="txtcustomer"  checked="checked"  value="customer"> <b>Customer</b> </label>
                                            <label class="btn btn-secondary">
                                            <input type="radio" name="txtuser" id="txtshop" value="shop"> <b> Hotels/Resturent</b> </label>
                                            <label class="btn btn-secondary">
                                            <input type="radio" name="txtuser" id="txtdriver" value="driver"> <b>Driver</b> </label>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                       <label for="txtfname">Full Name</label>
                                       <input class="form-control" type="text"  id="txtfname" name="txtfname"> 
                                    </div>
                                    
                                    <div class="form-group col-sm-6">
                                       <label for="txtnic">NIC.No</label>
                                       <input class="form-control" type="text" id="txtnic" name="txtnic">  
                                    </div>
                                    <div class="form-group col-sm-6">
                                       <label for="txtemail">Email address</label>
                                       <input type="email" class="form-control" id="txtemail" name="txtemail"  placeholder="Enter email">  
                                    </div>                              
                                    <div class="form-group col-sm-6">
                                       <label for="txtphone">Phone number</label>
                                       <input class="form-control" type="text"  id="txtphone" name="txtphone"> 
                                    </div>
                                                                                                            
                                    <div class="form-group col-sm-6">
                                       <label for="txtcpass">Confirm password</label>
                                       <input type="password" class="form-control" id="txtcpass" placeholder="Password" name="txtcpass"> 
                                    </div>
                                     <div class="form-group col-sm-6" hidden id="divlicence">
                                       <label for="txtlicence">Drive licence</label>
                                       <input class="form-control" type="text" id="txtlicence" name="txtlicence"> 
                                    </div>

                                    <div class="form-group col-sm-6">
                                       <label for="txtpass">Password</label>
                                       <input type="password" class="form-control" id="txtpass" placeholder="Password" name="txtpass"> 
                                    </div>
                                   
                                    <div class="form-group col-sm-6" hidden id="divvehicelno">
                                       <label for="txtvechicalno">Vehicle no</label>
                                       <input class="form-control" type="text"  id="txtvechicalno" name="txtvechicalno" > 
                                    </div>
                                    
                                     <div class="form-group col-sm-6" hidden id="divvehicletype">
                                       <label for="txtvechiletype">Vehicle type</label>
                                       <select class="form-control m-b-sm" name="txtvechiletype" id="txtvechiletype">
                                            <option value="bike">bike</option>
                                            <option value="car">car</option>
                                            <option value="three wheeler">three wheeler</option>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6" hidden id="divshopname">
                                       <label for="txtshopname">Shop Name</label>
                                       <input class="form-control" type="text"  id="txtshopname" name="txtshopname"> 
                                    </div>
                                    <div class="form-group col-sm-6" hidden id="divregno">
                                       <label for="txtregno">Reg.No</label>
                                       <input class="form-control" type="text" id="txtregno" name="txtregno"> 
                                    </div>

                                
                                 <div class="row">
                                    <div class="col-sm-4">
                                    <p> 
                                        <button type="submit" class="btn btn-success btn-rounded">Register</button>
                                      
                                     </p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           <!-- end: Widget -->
                        </div>

                        
                        <!-- /REGISTER -->
                        
                     </div>
                     <!-- WHY? -->
                     
                        
                     </div>
                     <div class="col-md-6">
                     <h3>LOGIN</h3>
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                   
                                </div>
                                <div class="panel-body">
                                    <form  method="POST" action="loginload.php" id="login" >
                                        <div class="form-group" data-validate = "Email address">
                                            <label for="exampleInputEmail1">Email address</label>
                                            
                                            <input type="email" class="form-control" name="txtusername" id="txtusername" placeholder="Enter email">
                                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" class="form-control" name="txtpassword" id="txtpassword" placeholder="Password">
                                        </div>

                                       <div class="contact100-form-checkbox">
                                          <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">  
                                          <label class="label-checkbox100" for="ckb1">Remember me</label>
                                       </div>

                                        <!-- <a herf="forgot.php">  -->
                                        <button type="button" class="btn btn-success btn-rounded">forget password</button>
                                        <button type="submit" name="login" class="btn btn-success btn-rounded login">LOGIN</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <!-- end:Panel -->
                        
                     <!-- /WHY? -->
                  </div>
               </div>
            </section>
            
            <!-- start: FOOTER -->
            <footer class="footer">
               <div class="container">
                  <!-- top footer statrs -->
                  <div class="row top-footer">
                     <div class="col-xs-12 col-sm-3 footer-logo-block color-gray">
                        <a href="#"> <img src="images/food-picky-logo.png" alt="Footer logo"> </a> <span>Order Delivery &amp; Take-Out </span> 
                     </div>
                     <div class="col-xs-12 col-sm-2 about color-gray">
                        <h5>About Us</h5>
                        <ul>
                           <li><a href="#">About us</a> </li>
                           <li><a href="#">History</a> </li>
                           <li><a href="#">Our Team</a> </li>
                           <li><a href="#">We are hiring</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-2 how-it-works-links color-gray">
                        <h5>How it Works</h5>
                        <ul>
                           <li><a href="#">Enter your location</a> </li>
                           <li><a href="#">Choose restaurant</a> </li>
                           <li><a href="#">Choose meal</a> </li>
                           <li><a href="#">Pay via credit card</a> </li>
                           <li><a href="#">Wait for delivery</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-2 pages color-gray">
                        <h5>Pages</h5>
                        <ul>
                           <li><a href="#">Search results page</a> </li>
                           <li><a href="#">User Sing Up Page</a> </li>
                           <li><a href="#">Pricing page</a> </li>
                           <li><a href="#">Make order</a> </li>
                           <li><a href="#">Add to cart</a> </li>
                        </ul>
                     </div>
                     <div class="col-xs-12 col-sm-3 popular-locations color-gray">
                        <h5>Popular locations</h5>
                        <ul>
                           <li><a href="#">Jaffna town</a> </li>
                           <li><a href="#">Nallur</a> </li>
                           <li><a href="#">kandy Road</a> </li>
                           <li><a href="#">Kokuvil</a> </li>
                           <li><a href="#">KSS Road</a> </li>
                           <li><a href="#">Kopay</a> </li>
                           <li><a href="#">Palai</a> </li>
                           
                        </ul>
                     </div>
                  </div>
                  <!-- top footer ends -->
                  <!-- bottom footer statrs -->
                  <div class="row bottom-footer">
                     <div class="container">
                        <div class="row">
                           <div class="col-xs-12 col-sm-3 payment-options color-gray">
                              <h5>Payment Options</h5>
                              <ul>
                                 <li>
                                    <a href="#"> <img src="images/paypal.png" alt="Paypal"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="images/mastercard.png" alt="Mastercard"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="images/maestro.png" alt="Maestro"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="images/stripe.png" alt="Stripe"> </a>
                                 </li>
                                 <li>
                                    <a href="#"> <img src="images/bitcoin.png" alt="Bitcoin"> </a>
                                 </li>
                              </ul>
                           </div>
                           <div class="col-xs-12 col-sm-4 address color-gray">
                              <h5>Address</h5>
                              <p>Concept design of oline food order and deliveye,planned as restaurant directory</p>
                              <h5>Phone: <a href="tel:+080000012222">080 000012 222</a></h5>
                           </div>
                           <div class="col-xs-12 col-sm-5 additional-info color-gray">
                              <h5>Addition informations</h5>
                              <p>Join the thousands of other restaurants who benefit from having their menus on TakeOff</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- bottom footer ends -->
               </div>
            </footer>
            <!-- end:Footer -->
         </div>
         <!-- end:page wrapper -->
      </div>
      <!--/end:Site wrapper -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/animsition.min.js"></script>
    <script src="js/bootstrap-slider.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/headroom.js"></script>
    <script src="js/foodpicky.min.js"></script>
    <script>
        $(document).ready(function(){
            // alert('hi');'
            $('.btn-secondary').click(function(){  //jquery 
                
             var user = $(this).find('input:radio[name=txtuser]').val();
                // alert(user);
             if(user == "shop")
             {
                 $('#divlicence').attr('hidden',true);
                 $('#divvehicelno').attr('hidden',true);
                 $('#divvehicletype').attr('hidden',true);
                 $('#divshopname').removeAttr('hidden');
                 $('#divregno').removeAttr('hidden');
             }
             else  if(user == "driver")
             {
                 $('#divlicence').removeAttr('hidden');
                 $('#divvehicelno').removeAttr('hidden');
                 $('#divvehicletype').removeAttr('hidden');
                 $('#divshopname').attr('hidden',true);
                 $('#divregno').attr('hidden',true);
             }
             else  if(user == "customer")
             {
                 $('#divlicence').attr('hidden',true);
                 $('#divvehicelno').attr('hidden',true);
                 $('#divvehicletype').attr('hidden',true);
                 $('#divshopname').attr('hidden',true);
                 $('#divregno').attr('hidden',true);
             }
            
            })
           
            $( "form" ).on( "submit", function(event) {
            
               event.preventDefault();
               // alert('hi');
               var data = $( this ).serialize();
               console.log( $( this ).serialize() );

               $.ajax({

                  method:'POST',
                  url:"../load/reg_save.php"
                  data:data,
                  dataType:"text",
                  success:function(data)
                  {
                     // $('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
                     // alert('Added Successfully')//piraku edit panu success message lable illa toastor alert ah maathi
                     console.log(data)
                     // alert(data.error);
                     // alert(data.success);
                  }

               })
            });

            
            $('.login').click(function(e){
               alert('fdfd');
               e.preventDefault();
               var data = $( this ).serialize();
               $.ajax({

                  method:'POST',
                  url:"/load/loginload.php"
                  data:data,
                  dataType:"text",
                  success:function(data)
                  {
                     // $('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
                     // alert('Added Successfully')//piraku edit panu success message lable illa toastor alert ah maathi
                     console.log(data)
                     // alert(data.error);
                     // alert(data.success);
                  }

               })
            })

        });

        
    </script>

<th>Table heading</th>
 <th>Table heading</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											wait

	</div><!-- Row -->
</div><!-- Main Wrapper -->


<script>
$(document).ready(function(){
   alert(hi);
	
});
 </script>   
</body>


</html>