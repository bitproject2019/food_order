<div class="page-sidebar sidebar">
	<div class="page-sidebar-inner slimscroll">
		<div class="sidebar-header">
			<div class="sidebar-profile">
				<a href="javascript:void(0);" id="profile-menu-link">
					<div class="sidebar-profile-image">
						<img src="assets/images/profile-menu-image.png" class="img-circle img-responsive" alt="">
					</div>
					<div class="sidebar-profile-details">
						<span>lavarinthan<br><small>ADMIN</small></span>
					</div>
				</a>
			</div>
		</div>
		<ul class="menu accordion-menu">
			<?php
				if($_SESSION['user_type'] == "driver")
				{
			?>
					<li><a href="all-order.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-th-list"></span><p>Dashboard</p></a></li>
					<li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-home"></span><p>Food Shops</p></a></li>
					<li><a href="driver-profile.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-send"></span><p>Profile</p></a></li>
					<li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-user"></span><p>Coustomer</p></a></li>
					
					<!-- <li><a href="all-order.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Order</p></a></li> -->
					<li><a href="driver-order.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Order Status</p></a></li>
					<li><a href="food-view-driver.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Food</p></a></li>
			<?php
				}
				else if($_SESSION['user_type'] == "shop")
				{
			?>
					<li><a href="add-food.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-th-list"></span><p>Dashboard</p></a></li>
					<!-- <li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-home"></span><p>Food Shops</p></a></li> -->
					<li><a href="profile.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-send"></span><p>Profile</p></a></li>
					<!-- <li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-user"></span><p>Coustomer</p></a></li>
					 -->
					<li><a href="add-food.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Food</p></a></li>
					<li><a href="food-view-driver.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Food View</p></a></li>
					<li><a href="all-feedback.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Feedback</p></a></li>
			<?php		
				}
				else if($_SESSION['user_type'] == "customer")
				{
			?>
					<li><a href="index-2.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-th-list"></span><p>Dashboard</p></a></li>
					<li><a href="food-view-driver.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-home"></span><p>Food Shops</p></a></li>
					<li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-send"></span><p>RIDER</p></a></li>
					<li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-user"></span><p>Coustomer</p></a></li>
					
					<li><a href="all-order.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Order</p></a></li>
					<li><a href="feedback.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Feedback</p></a></li>
			<?php		
				}
			
				else if($_SESSION['user_type'] == "admin")
				{
			?>
				<li><a href="index-2.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-th-list"></span><p>Dashboard</p></a></li>
				<li><a href="add-food-admin.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-home"></span><p>Food Shops</p></a></li>
				<li><a href="food-view-driver.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Food View</p></a></li>
				<li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-send"></span><p>RIDER</p></a></li>
				<li><a href="profile.html" class="waves-effect waves-button"><span class="glyphicon glyphicon-user"></span><p>Coustomer</p></a></li>
				
				<li><a href="all-order.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Order</p></a></li>
				<li><a href="all-order-status.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Order Status</p></a></li>
				<li><a href="all-feedback.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Feedback</p></a></li>
				<li><a href="all-driver.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Driver</p></a></li>
				<li><a href="all-shop.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-tasks"></span><p>Shop</p></a></li>
			<?php		
				}
			?>

		</ul>
			
	</div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->