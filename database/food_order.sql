-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 04, 2019 at 09:05 AM
-- Server version: 10.3.12-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `food_order`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL,
  `nic_no` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` int(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `full_name`, `nic_no`, `email`, `phone_number`, `user_id`) VALUES
(6, 'lavarinthan', '971252901v', 'lava@gmail.com', 75548524, 13),
(8, '', '', '', 0, 1),
(9, '', '', '', 0, 16),
(15, 'nirosha', '958012381v', 'nirosha@gmail.com', 771289586, 15),
(16, '', '958012381v', 'nirosha@gmail.com', 787878771, 24),
(17, '', '958012381v', 'nirosha@gmail.com', 787878771, 25),
(18, '', '958012381v', 'nirosha@gmail.com', 771289586, 26),
(19, '', '958012381v', 'nirosha@gmail.com', 11, 27);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

DROP TABLE IF EXISTS `driver`;
CREATE TABLE IF NOT EXISTS `driver` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `nic_no` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `vehicle_type` varchar(20) NOT NULL,
  `drive_licence` varchar(20) NOT NULL,
  `vehicle_no` varchar(20) NOT NULL,
  `phone_number` int(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`id`, `full_name`, `nic_no`, `email`, `vehicle_type`, `drive_licence`, `vehicle_no`, `phone_number`, `user_id`) VALUES
(1, 'lava', 'a', 'dsadada@sfsf.sfsafsf', 'car', 'a', '23123', 323, 14);

-- --------------------------------------------------------

--
-- Table structure for table `driver_duty`
--

DROP TABLE IF EXISTS `driver_duty`;
CREATE TABLE IF NOT EXISTS `driver_duty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `duty` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_duty`
--

INSERT INTO `driver_duty` (`id`, `driver_id`, `duty`, `date`) VALUES
(2, 14, 'leave', '2019-05-04 07:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `driver_order`
--

DROP TABLE IF EXISTS `driver_order`;
CREATE TABLE IF NOT EXISTS `driver_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_order`
--

INSERT INTO `driver_order` (`id`, `driver_id`, `order_id`, `date`) VALUES
(1, 14, 4, '2019-05-03 16:57:30'),
(3, 14, 5, '2019-05-04 06:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
CREATE TABLE IF NOT EXISTS `food` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `price` varchar(100) NOT NULL,
  `discount` int(11) NOT NULL,
  `discount_type` varchar(100) NOT NULL,
  `shop_name` varchar(100) NOT NULL,
  `shop_location` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `name`, `time`, `price`, `discount`, `discount_type`, `shop_name`, `shop_location`, `image`, `date`) VALUES
(1, 'Kottu', 'Breakfast', '12', 12, 'Percentage', 'rolex', '12', '561.PNG', '2019-05-03 16:18:25'),
(2, 'rice', 'Dinner', '12', 1, 'Percentage', 'Sanmugam', '12', '400.png', '2019-05-03 16:18:42'),
(3, 'puttu ', 'Dinner', '80', 0, 'Percentage', 'taj', 'A9 Road ', '671.jpg', '2019-04-28 04:08:03');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `user_type`) VALUES
(13, 'a', 'a', 'admin'),
(14, 'd', 'd', 'driver'),
(15, 'c', 'm', 'customer'),
(16, 's', 's', 'customer'),
(17, 'a', '', 'driver'),
(18, 'a', '', 'driver'),
(19, '', '', 'driver'),
(20, 'a', '', 'driver'),
(21, 'a', '', 'driver'),
(22, 'a', '', 'driver'),
(23, 'dsadada@sfsf.sfsafsf', '', 'driver'),
(24, 'nirosha@gmail.com', '', 'customer'),
(25, 'nirosha@gmail.com', '', 'customer'),
(26, 'nirosha@gmail.com', '', 'customer'),
(27, 'nirosha@gmail.com', '', 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `total_rate` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `temp_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `food_id`, `order_id`, `location`, `total_rate`, `quantity`, `date`, `user_id`, `status`, `temp_id`) VALUES
(4, 1, '#4036086160', NULL, '10.56', 1, '2019-05-03 16:59:34', 15, 'Taken', '509972'),
(5, 1, '#154006682', NULL, '10.56', 1, '2019-05-04 06:57:59', NULL, 'Taken', '360293'),
(6, 2, '#1914727613', NULL, '10.56', 1, '2019-05-03 16:53:51', 15, 'Basket', '360293'),
(7, 1, '#3320445208', NULL, '10.56', 1, '2019-05-03 16:10:40', NULL, 'Basket', '360293'),
(15, 2, '#2896991837', 'fasfsf', '154.44', 13, '2019-05-03 16:53:44', 15, 'Basket', '109975'),
(16, 3, '#4248073301', NULL, '80', 1, '2019-05-04 07:01:52', 14, 'Basket', '456201');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `amount` varchar(60) NOT NULL,
  `type` varchar(60) NOT NULL,
  `status` varchar(60) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `token` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `order_id`, `amount`, `type`, `status`, `date`, `token`, `user_id`) VALUES
(1, 15, '154.44', 'Cash', 'No Paid', '2019-05-03 10:00:29', '#2896991837', 15);

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
CREATE TABLE IF NOT EXISTS `shop` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL,
  `nic_no` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` int(50) NOT NULL,
  `shop_name` varchar(20) NOT NULL,
  `reg_no` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_location` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`id`, `full_name`, `nic_no`, `email`, `phone_number`, `shop_name`, `reg_no`, `user_id`, `shop_location`, `image`) VALUES
(1, 'lavari', 'asa', 'adasd@wssds.sfsfsf', 7678345, 'taj', 'wertythj', 13, 'wewqeqwe', '834.jpg'),
(2, 'hjg', 'hggh', 'gkghkggh', 7686, '12', '69876', 15, 'sadasd', '89.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
