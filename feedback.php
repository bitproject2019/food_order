<?php
session_start();

?>
<?php 
include_once('header.php');
include_once('sidebar.php');

?>


<div class="page-inner">
	<div class="page-title">
		<h3>Feedback</h3>
		<div class="page-breadcrumb">
			<ol class="breadcrumb">
				
				
			</ol>
		</div>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">ADD FEEDBACK</h4>
			</div>
			 
			 <div class="panel-body">
				<form class="form-horizontal" method="POST" >
                    <div class="form-group">
						<label for="txtfood" class="col-sm-2 control-label">Food</label>
						<div class="col-sm-10">
                            <select class="form-control m-b-sm" name="txtfood" id="txtfood" required>
								<option value="">Choose</option>
							

                                
                            </select>
                        </div>
					</div>
					<div class="form-group">
						<label for="txtrating" class="col-sm-2 control-label">Rating</label>
						<div class="col-sm-10">
                            <select class="form-control m-b-sm" name="txtrating" id="txtrating">
								<option value="5 star">5 Star</option>
								<option value="4 star">4 Star</option>
								<option value="3 star">3 Star</option>
								<option value="2 star">2 Star</option>
								<option value="1 star">1 Star</option>

                                
                            </select>
                        </div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="txtcomments">Comments</label>
						<div class="col-sm-10">
							<textarea name="txtcomments" id="txtcomments" cols="30" rows="10" class="form-control"></textarea>
							
						</div>
					</div>
					
				
					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-5">
							<button type="submit" class="btn btn-success btn-addon m-b-sm"><i class="fa fa-plus"></i> ADD TO WEBSITE</button>

						</div>
					</div>
			</form>	
		</div>	
	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
    $.ajax({

        method:'POST',
        url:"load/feedback-food.php",
        //data:,
        dataType:"text",
        success:function(data)
        {
            $('#txtfood').append(data);
        }

    })

    $( "form" ).on( "submit", function( event ) {
		event.preventDefault();
		
		var data = $( this ).serialize();
		// console.log( $( this ).serialize() );
		// console.log(form_data);
		$.ajax({

			method:'POST',
			url:"load/feedback.php",
			// data:{form_data:form_data,data:data},
			data: new FormData(this),
			// dataType:"text",
			contentType:false,
			cache:false,
			processData:false,
			success:function(data)
			{
				//console.log(data);
				$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
				alert('Added Successfully')//piraku edit panu success message lable illa toastor alert ah maathi
				// console.log(data)
			}

		})
	});

})
</script>
<?php 
    include_once('footer.php');
?>



