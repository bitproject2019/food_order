<?php
session_start();
//echo $_SESSION['user_id'];
//$_SESSION['user_id'] = $user_id;
if (!isset($_SESSION['user_id'],$_SESSION['user_type'])) {
    header('location:foodpicky/registration.php');
    exit;
}

?>
<?php 
include_once('header.php');
include_once('sidebar.php');
$connect = mysqli_connect("localhost", "root", "", "food_order");  
$query = "SELECT * FROM food";  
$result = mysqli_query($connect, $query);
?>


<div class="page-inner">
	<div class="page-title">
		<h3>Admin</h3>
		<div class="page-breadcrumb">
			<ol class="breadcrumb">
				
				
			</ol>
		</div>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">ADD FOOD</h4>
			</div>
			 
			 <div class="panel-body">
				<form class="form-horizontal" method="POST" >
					<div class="form-group">
						<label for="txtfood" class="col-sm-2 control-label">Food Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtfood" name="txtfood">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="txttime">Time</label>
						<div class="col-sm-10">
							<select class="form-control m-b-sm" multiple name="txttime" id="txttime">
								<option value="Breakfast">BREAKFAST</option>
								<option value="Lunch">LUNCH</option>
								<option value="Tea Time">TEA TIME</option>
								<option value="Dinner">DINNER</option>
							</select>
							
						</div>
					</div>
					<div class="form-group">
						<label for="txtshopname" class="col-sm-2 control-label">Shop Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtshopname" name="txtshopname">
						</div>
						</div>

						<div class="form-group">
						<label for="txtshopaddress" class="col-sm-2 control-label">Shop Location</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtshopaddress" name="txtshopaddress">
						</div>
					</div>
					<div class="form-group">
						<label for="txtprice" class="col-sm-2 control-label">Food Price</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtprice" name="txtprice">
						</div>
					</div>

					<div class="form-group">
						<label for="txtdiscount" class="col-sm-2 control-label">Discount</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="txtdiscount" name="txtdiscount">
						</div>
						<div class="col-sm-5">
							<select class="form-control m-b-sm" name="txtdiscounttype" id="txtdiscounttype">
								<option value="Percentage">%</option>
								<option value="Rupees">RS</option>
								
							</select>
							
						</div>
					</div>

								
    				<div class="form-group">
						<label for="txtimage" class="col-sm-2 control-label">Add Image</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" id="file" name="file">
						</div>
					</div>
				
					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-5">
							<button type="submit" class="btn btn-success btn-addon m-b-sm"><i class="fa fa-plus"></i> ADD TO WEBSITE</button>

						</div>
					</div>
			</form>	
		</div>	
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Responsive table</h4>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Time</th>
									<th>Price</th>
									<th>Discount</th>
									<th>Discount Type</th>
									<th>Shop Name</th>
									<th>Shop Location</th>
									<th>Shop Image</th>
									<th>Action</th>


								</tr>
							</thead>
							<tbody id="tbody">
							</tbody>
							<tfoot>
							</tfoot>
						</table>
										
					</div>
				</div><!-- Row -->
			</div>
<!--Main Wrapper-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
	$.ajax({

		method:'POST',
		url:"load/view.php",
		//data:,
		dataType:"text",
		success:function(data)
		{
			$('#tbody').html(data);
		}

	})
	
	$( "form" ).on( "submit", function( event ) {
		event.preventDefault();
		// var result= 'food';
		// var property = document.getElementById('file').files[0];
		// // console.log(property);
		// var image_name = property.name;
		// var image_extension = image_name.split('.').pop().toLowerCase();
		// if(jQuery.inArray(image_extension,['gif','png','jpg','jpeg'])== -1)
		// {
		// 	alert('invalid image file');
		// }
		// var image_size = property.size;
		// if(image_size>2000000)
		// {
		// 	alert('image size error');
		// }
		// else{
		// 	var form_data = new FormData();
		// 	form_data.append('file',property);

		// }
		var data = $( this ).serialize();
		// console.log( $( this ).serialize() );
		// console.log(form_data);
		$.ajax({

			method:'POST',
			url:"load/save.php",
			// data:{form_data:form_data,data:data},
			data: new FormData(this),
			// dataType:"text",
			contentType:false,
			cache:false,
			processData:false,
			success:function(data)
			{
				//console.log(data);
				$('form input[type="text"],input[type="email"],input[type="password"],texatrea').val('');
				alert('Added Successfully')//piraku edit panu success message lable illa toastor alert ah maathi
				// console.log(data)
			}

		})
	});
});
$(document).ready(function(e) {
	// var duty = $('#duty').attr('checked',true);
	// console.log(duty);
});
</script>
<?php 
    include_once('footer.php');
?>



