<?php
session_start();
//echo $_SESSION['user_id'];
//$_SESSION['user_id'] = $user_id;
if (!isset($_SESSION['user_id'],$_SESSION['user_type'])) {
    header('location:foodpicky/registration.php');
    exit;
}

?>
<?php 
include_once('header.php');
include_once('sidebar.php');

?>


<div class="page-inner">
	<div class="page-title">
		<h3>Admin</h3>
		<div class="page-breadcrumb">
			<ol class="breadcrumb">
				
				
			</ol>
		</div>
	</div>
	<div id="main-wrapper">
		<div class="row">
			<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">Orders</h4>
			</div>
			 
			 <div class="panel-body">
			
		</div>	
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Order Details</h4>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Licence</th>
									<th>Vehicel Type</th>
									<th>Vehicel No</th>
                                    <th>Phone No</th>
                                    <th>Verification</th>
									<th>Action</th>


								</tr>
							</thead>
							<tbody id="tbody">
							</tbody>
							<tfoot>
							</tfoot>
						</table>
										
					</div>
				</div><!-- Row -->
			</div>
<!--Main Wrapper-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(e){
	
	$.ajax({

		method:'POST',
		url:"load/view-driver.php",
		//data:,
		dataType:"text",
		success:function(data)
		{
			$('#tbody').html(data);
		}

	});

	
})

</script>
<?php 
    include_once('footer.php');
?>



